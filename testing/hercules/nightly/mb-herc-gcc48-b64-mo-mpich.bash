bname=$(basename "$0")
bname=${bname/.bash/}
##########
source /opt/mpich-3.1.4-gnu-4.8/source.bash

wrk=$1
lib=ScaTeLib_$bname
if [ ! -d $wrk/$lib ]
then
   cd $wrk
   git clone git@gitlab.com:pett/ScaTeLib.git $lib
fi
#
cd $wrk/$lib
#
if [ ! -d $wrk/$lib/$bname ]   
then
   ./setup --fc=mpif90 --omp --int64 --cmake-options="-DBUILDNAME=$bname" $bname
fi
#
cd $bname
#
ctest -D Nightly
