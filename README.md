# ScaTeLib - a scalable tensor library

## Overview

This project provides a flexible implementation of a tensor type in the fortran language.
The two main types of tensors currently implemented are dense tensors and parallel distributed
tensors and BLAS-1 like operations for the as well as tensor contractions for both tensor types.

## Installation

This project has been set up using
[Autocmake](http://autocmake.org) and provides the
CMakeLists.txt and a setup.py script for the configuration. A simple debug
build without MPI may be invoked as

    ./setup.py --type=debug; cd build; make

Building a release version with MPI and OpenMP would be done the following way

    ./setup.py --type=release --fc=mpif90 --mpi --omp; cd build; make

ctest has been implemented and the current executable obtained from the upper
builds supports three testcases (z=1,2,3) which can be run individually (on y
nodes) with

    (mpirun -n y) ./src/TENSOR\_LIB.x z

or all of them through ctest

    ctest

## Documentation

The Documentation is included in the doc directory: ``doc.tex``. It should be directly compilable with ``pdflatex`` but a Makefile is included for conveniece.

## Developers

- Patrick Ettenhuber (email: pettenhuber@gmail.com)
- Dmitry Liakh       (email: quant4me@gmail.com)

### Other contributions

- [Radovan Bast](http://bast.fr)
- Thomas Kjaergaard
- Janus Juul Eriksen
