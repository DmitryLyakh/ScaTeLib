!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     February, 2016
! File:     This file contains a simple example of how to use ScaTeLib
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Compile: where ../../build has to be replaced by your specific build path and
! the arguments in parenthesis are only necessary if you compiled ScaTeLib with
! these, conversely this means that you need to provide every lib you used for
! compiling ScaTeLib when linking this example:

! Compile: mpif90 -I../../build/modules  -c example3bmpi.F90 -o example3bmpi.F90.o
! Link:    mpif90 ../../build/src/CMakeFiles/ScaTeLib.dir/tensor_interface.F90.o example3bmpi.F90.o -o example3bmpi.x  ../../build/src/libScaTeLib.a ( -framework Accelerate -fopenmp )

module test_module
   use mpi
   use tensor_interface_module

   integer, parameter :: job1=1
   integer, parameter :: job2=2
   integer, parameter :: job_tensor_sync=3
   integer, parameter :: job_exit=4
#ifdef VAR_INT64
  integer(tensor_mpi_kind), parameter :: TMPII = MPI_INTEGER8
#else
  integer(tensor_mpi_kind), parameter :: TMPII = MPI_INTEGER
#endif

   contains

subroutine slaves_routine()
   implicit none
   integer :: job
   integer(tensor_mpi_kind), parameter :: root = 0
   integer(tensor_mpi_kind) :: ierr
   logical :: stay

   stay = .true.

   do while( stay )
      call mpi_bcast(job,1_tensor_mpi_kind,TMPII,root,MPI_COMM_WORLD,ierr)

      select case(job)
      case(job1)
         print *, "Slave got job 1"
      case(job2)
         print *, "Slave got job 2"
      case(job_tensor_sync)
         print *, "Slave got job pdm_tensor_slave"
        call pdm_tensor_slave
      case(job_exit)
         print *, "Slave got job exit"
         stay = .false.
      case default
         print *,"Error, unknown job"
      end select

   enddo

end subroutine slaves_routine


subroutine master_routine()
   implicit none
   type(tensor)       :: A,B,C
   integer, parameter :: d1 = 140
   integer, parameter :: d2 = 144
   integer, parameter :: d3 = 124
   integer, parameter :: d4 = 180
   integer, parameter :: s1 = 12
   integer, parameter :: s2 = 14
   integer, parameter :: s3 = 8
   integer, parameter :: s4 = 20
   real(tensor_dp), pointer :: work(:)
   type(buffer_spec) :: buf
   integer(tensor_mpi_kind), parameter :: root = 0
   integer(tensor_mpi_kind) :: ierr, me, nn

   call mpi_comm_rank(mpi_comm_world,me,ierr)
   call mpi_comm_size(mpi_comm_world,nn,ierr)

   !please feel free to test any tensor operation here!
   call tensor_minit(A,[d1,d2,d3,d4],tdim=[s1,s2,s3,s4],Ttype=TT_TiledDistributedTensor,local=(nn==1))
   call tensor_minit(B,[d4,d1,d2],   tdim=[s4,s1,s2],   Ttype=TT_TiledDistributedTensor,local=(nn==1))
   call tensor_minit(C,[d1,d3,d1],   tdim=[s1,s3,s1],   Ttype=TT_TiledDistributedTensor,local=(nn==1))

   call tensor_random(A)
   call tensor_random(B)
   call tensor_random(C)

   ! some buffer size
   allocate(work(5*s1*s2*s3*s4))

   ! initialize the buffering information, here you can specify a work space or
   ! the amount of memory the contraction routine may allocate. If given both,
   ! as here, the routine will choose the largest of the two possibilities.
   ! Using both at the same time is currently not possible: Choose your poison
   ! carefully
   !call buf%init(wrk = work, mem = 1073741824_tensor_long_int )

   ! A simple example for a tensor contraction with scaling of the product
   call tensor_contract(C,'Bar(eca) = beta*alpha*Foo1(abcd)Foo2(deb) + Bar(eca)',A,B,alpha=2.0D0,beta=8.0D0)

   !call buf%free()

   call print_norm(C,'Result norm:',print_=.true.)

   call tensor_free(A)
   call tensor_free(B)
   call tensor_free(C)

   ! Tell the slaves to exit
   call mpi_bcast(job_exit,1_tensor_mpi_kind,TMPII,root,MPI_COMM_WORLD,ierr)
end subroutine master_routine

end module test_module




program simple_tensor_use
   use mpi
   use tensor_interface_module
   use test_module
   implicit none
   integer(kind=8)    :: bytes_used  = 0
   integer, parameter :: mpi32o64bit = 4

   !If you have used a 64bit MPI library to compile ScaTeLib use the following line in
   !integer, parameter :: mpi32o64bit = 8
   integer(kind=mpi32o64bit) :: ierr, me, nn

   !Initialize MPI before you Initialize the tensors
   call mpi_init(ierr)
   call mpi_comm_rank(mpi_comm_world,me,ierr)
   call mpi_comm_size(mpi_comm_world,nn,ierr)

   print *,"on rank",me,"of",nn,"Before initialization, bytes_used:   ",bytes_used

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Tensor operations will not work here !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !Initialize the tensor intrerface
   call tensor_initialize_interface(mpi_comm_world, mem_ctr = bytes_used, pdm_slaves_signal = job_tensor_sync)

   if( me == 0 )then
      call master_routine()
   else
      call slaves_routine()
   endif

   ! Finalize the tensor interface
   call tensor_finalize_interface

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Tensor operations will not work here !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !Finalize MPI after you Initialize the tensors
   call mpi_finalize(ierr)

end program simple_tensor_use
