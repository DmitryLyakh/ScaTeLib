     integer(tensor_mpi_kind), pointer     :: nnod => null()        ! number of nodes
     integer(tensor_mpi_kind), pointer     :: comm => null()    ! communicator
     integer(tensor_standard_int), pointer :: offset => null() ! use offset in nodes for the distribution of arrays
     integer(tensor_standard_int), pointer :: access_type      ! type of access to the array
     integer(tensor_standard_int), pointer :: addr_p_arr(:) => null()     !address of array in persistent array "p_arr" on each compute node
