      character*(*),intent(in) :: msg
      call tensor_error_preamble()
      if(tensor_stack%c<1)then
         print *, error_b//"unknown"//error_e//adjustl(trim(msg))
         print *,"The most recently called functions on the stack were:"
         print *, trim(tensor_stack%s(1))
         print *, trim(tensor_stack%s(2))
         print *, trim(tensor_stack%s(3))
      else
         print *, error_b//trim(tensor_stack%s(tensor_stack%c))//error_e//adjustl(trim(msg))
      endif

      call tensor_print_stack()

      print *, "Tensor status quit has been called with:",stat
      stop 1
