
     integer :: order(B%mode)
     logical :: test_all_master_access,test_all_all_access,master,contraction_mode
     real(tensor_dp), pointer :: buffB(:,:),wA(:),wB(:),dummy(:)
     integer :: ibufB, ntens_to_get_from, tsizeB, nbuffsB
     integer :: gc, ro(B%mode)
     integer :: mA(A%mode), mB(B%mode), tdimA(A%mode), tdimB(B%mode), ordB(B%mode)
     integer :: cmidA, cmidB
     integer(tensor_standard_int) :: ntpmB(B%mode)
     integer :: tdim_ord(B%mode), dims_ord(B%mode), ntpm_ord(B%mode), mB_ord(B%mode)
     integer :: nelmsTA, nelmsTB
     integer :: i,j,k,l, sq
     integer(tensor_long_int), parameter :: one = 1
     integer :: ltA,nltiB
     logical :: B_dense, B_rep, locA, locB, found , simple, fs
     real(tensor_dp), pointer :: w(:), t(:)
     real(tensor_dp) :: res 
     integer(tensor_long_int) :: itest
     integer(tensor_mpi_kind), parameter :: root = 0
     integer(tensor_mpi_kind) :: nnod
     integer(tensor_mpi_kind) :: me
     ! Buffer handling
     type(buffer_spec)         :: buf_
     type(tensor_async_buffer) :: asyncB
     call tensor_stack_push(srname)

     res = 0.0E0_tensor_dp
     fs  = .true.
     if(present(force_sync))fs=force_sync

     call tensor_get_rank(me, master)
     call tensor_get_size(nnod)

     if(present(buf))then
        buf_ = buf
     else
        call tensor_buffer_spec_init(buf_)
     endif

     order = o
     simple = .true.
     do i = 1, B%mode
        if( order(i) /= i )simple = .false. 
        ordB(i)  = i
     enddo

     test_all_master_access = (A%mpi%access_type == AT_Master)
     test_all_all_access    = (A%mpi%access_type == AT_All)

     if(  (.not.test_all_master_access.and..not.test_all_all_access) .or. &
        & (     test_all_master_access.and.     test_all_all_access)  )then
        call tensor_status_quit("Invalid access types",-1)
     endif

     !calculate reverse order
     do i = 1, B%mode
        ro(order(i)) = i
     enddo

     select type(B)
     class is (DenseTensor)

        B_dense = .true.

        do i = 1, B%mode
           tdimB(i) = A%tdim(o(i))
           ntpmB(i) = A%ntpm(o(i))
        enddo

        tsizeB = product(tdimB)

        ntens_to_get_from = 0
        nbuffsB           = 0

     type is (TiledDistributedTensor)

        B_dense = .false.

        ntpmB   = B%ntpm
        tdimB   = B%tdim
        tsizeB  = B%tsize

        !TODO: Optimize number of buffers for A and B
        ntens_to_get_from = 1
        nbuffsB           = 2

     class default
        call tensor_status_quit("B needs to be DenseTensor or TiledDistributedTensor",-1)
     end select

     do i = 1, B%mode
        if(tdimB(i) /= A%tdim(o(i)))then
           print *,"tdim in mode",i,tdimB(i),A%tdim(o(i))
           call tensor_status_quit("tdim for B does not match with A",-1)
        endif
     enddo

     B_rep = .false.
     select type(B)
     type is (DenseReplicatedTensor)
        B_rep = .true.
     end select

     if(master.and.test_all_master_access)then
        if(.not. B_dense .or. B_rep)then
           call pdm_tensor_sync(JOB,A,B)

           call tensor_buffer(B%mode,root=root,comm=tensor_work_comm)
           call tensor_buffer(order,B%mode)
           call tensor_buffer(fs)
           call tensor_buffer(pre1)
           call tensor_buffer(pre2,finalize=.true.)

        else
           call pdm_tensor_sync(JOB_BDENSE,A)

           call tensor_buffer(B%mode,root=root,comm=tensor_work_comm)
           call tensor_buffer(order,B%mode)
           call tensor_buffer(fs)
           call tensor_buffer(pre1)
           call tensor_buffer(pre2)
           call tensor_buffer(B%dims,B%mode)
           call tensor_buffer(B%p,B%nelms, finalize=.true.)
        endif
     endif

     !calculate number of buffers from possible buffer size
     if(B_dense)then
        nbuffsB = 0
     else
        nbuffsB = 3
     endif

     !Desired buffer size
     itest = tsizeB * nbuffsB + tsizeB 
     call tensor_buffer_spec_set(buf_,[one * tsizeB * nbuffsB, one * tsizeB], p1=w, p2=wB)

     if(B_dense)then
        buffB => null()
     else
        buffB(1:tsizeB,1:nbuffsB) => w(1:tsizeB*nbuffsB)
     endif
     
     locB = .false.

     if( alloc_in_dummy )then

        select type (B)
        type is (TiledDistributedTensor)
           locB = B%lock_set(1)
           if(.not.locB)call BasicTensor_lock_wins(B,'s',all_nodes=.true.)
        end select

     endif

     if(.not.B_dense)then
        call tensor_async_buffer_initialize(asyncB,nbuffsB)
     endif


     select type(B)
     type is (TiledDistributedTensor)
        if( .not. (B%mpi%offset == A%mpi%offset.and.simple) )then
           call fill_buffer_for_blas1_like(asyncB,0,A,B,buffB,o,.true.)
        endif
     end select


     !loop over local tiles of A and contract corresponding 
     LocalTiles: do ltA = 1, A%nlti

        !get the global combined and global mode indices of the current C tile
        gc = A%ti(ltA)%gt

        call get_midx(gc,mA,A%ntpm,A%mode)

        do i=1,B%mode
           mB(i) = mA(o(i))
        enddo

        !get number of elements in tiles for A and B
        tdimA   =  A%ti(ltA)%d
        nelmsTA =  A%ti(ltA)%e
        wA      => A%p(1+(ltA-1)*A%tsize:nelmsTA+(ltA-1)*A%tsize)

        cmidB   = get_cidx(mB, ntpmB,B%mode)
        
        select type(B)
        class is (DenseTensor)

           cmidA   = get_cidx(mA,A%ntpm,A%mode)

           call tile_from_fort(1.0E0_tensor_dp,B%p,B%dims,int(B%mode),0.0E0_tensor_dp,wB,cmidA,int(A%tdim),ro)

        type is (TiledDistributedTensor)

           do i=1,B%mode
              tdimB(i) = tdimA(o(i))
           enddo

           ibufB = 1


           if( B%mpi%offset == A%mpi%offset .and. simple )then
              dummy => B%p(1+(ltA-1)*A%tsize:nelmsTA+(ltA-1)*A%tsize)
           else
              ! find tile in buffer
              call tensor_async_buf_find_tile_pos_in_buf(asyncB,int(cmidB),ibufB,found)
              if( .not. found) call tensor_status_quit("B tile must be present",-1)  
              ! flush the window
              call tensor_async_buf_make_sure_tile_is_here(asyncB,B,cmidB)

              dummy => buffB(1:nelmsTA,ibufB)
           endif

           if( simple )then
              wB => dummy(1:nelmsTA)
           else
              call array_reorder(1.0E0_tensor_dp,dummy,tdimB,ro,0.0E0_tensor_dp,wB)
              ! load new tiles
              call fill_buffer_for_blas1_like(asyncB,ltA,A,B,buffB,o,.true.)
           endif


        end select
