      integer(tensor_mpi_kind)  :: alloc_stat 
      integer(MPI_ADDRESS_KIND) :: b
      integer(tensor_mpi_kind)  :: info
      call tensor_stack_push(srname)

      info = MPI_INFO_NULL

      alloc_stat = 0

      !bytes are the number of elements times the size
      b = int(n * counters(idx)%size_,kind=MPI_ADDRESS_KIND)

      call MPI_ALLOC_MEM(b,info,c,alloc_stat)

      !$OMP CRITICAL
      counters(idx)%curr_ = counters(idx)%curr_ + b
      counters(idx)%high_ = max(counters(idx)%high_,counters(idx)%curr_)
      tensor_counter_max_hp_mem = max(tensor_counter_max_hp_mem,tensor_get_total_heap_mem())
      if(associated(tensor_counter_ext_mem)) tensor_counter_ext_mem = tensor_counter_ext_mem + b
      !$OMP END CRITICAL
         

      if( alloc_stat /= 0 ) then
         if(.not. present(stat))then
            call tensor_status_quit("allocation failed",alloc_stat)
         else
            stat = alloc_stat
         endif
      endif

      call c_f_pointer(c,p,[n])
      call tensor_stack_pop()
