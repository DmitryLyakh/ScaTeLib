module tensor_type_allocator_module

   use tensor_error_handler
   use tensor_parameters_module
   use tensor_counters_module
   use tensor_bg_buf_module
   use tensor_mpi_binding_module
   use tensor_tile_type_module
   use tensor_type_def_module
   use tensor_allocator_module

   public :: tensor_init_counters

   private

   contains

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   late 2015
  !Intent: intialize the counters of ScaTeLib
  subroutine tensor_init_counters()
     implicit none
     type(tile)   :: ref_tile
     call tensor_stack_push("tensor_init_counters")

     call tensor_init_specific_counter(counters,tensor_nmem_idx)
     call tensor_init_specific_counter(counters_bg,tensor_nmem_idx)
#ifdef VAR_MPI
     call tensor_init_mpi_counter(tensor_mpi_stats,tensor_nmpi_idx,tensor_nmpi_dat)
#endif
     call tensor_stack_pop()
  end subroutine tensor_init_counters

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   beginning 2015
  !Intent: intialize the counters of ScaTeLib
  !Input:
  !       -l: number of counters to initialize
  !Output:
  !       -c: tensor counter array
  subroutine tensor_init_specific_counter(c,l)
     implicit none
     integer(tensor_standard_int), intent(in) :: l
     type(tensor_counter_type) :: c(l)
     integer(tensor_standard_int) :: i
#ifdef VAR_MPI
     integer(MPI_ADDRESS_KIND) :: sze,lb
     integer(tensor_mpi_kind)  :: ierr
#endif
     call tensor_stack_push("tensor_init_specific_counter")
     do i=1,l

        !DEFINE THE SIZE OF ONE UNIT
        select case(i)
        !ATOMIC TYPES
        case(tensor_mem_idx_tensor_dp)
           c(i)%size_ = tensor_dp
        case(tensor_mem_idx_tensor_dp_mpi)
#ifdef VAR_MPI
           call MPI_TYPE_GET_EXTENT(MPI_DOUBLE_PRECISION,lb,sze,ierr)
           c(i)%size_ = sze
#else
           c(i)%size_ = 0
#endif
        case(tensor_mem_idx_tensor_standard_int)
           c(i)%size_ = tensor_standard_int
        case(tensor_mem_idx_tensor_long_int)
           c(i)%size_ = tensor_long_int
        case(tensor_mem_idx_tensor_standard_log)
           c(i)%size_ = tensor_standard_log
        case(tensor_mem_idx_tensor_long_log)
           c(i)%size_ = tensor_long_log
        case(tensor_mem_idx_character)
           c(i)%size_ = tensor_char_size

           !DERIVED TYPES
        case(tensor_mem_idx_tile)
           c(i)%size_ = tensor_bytes_per_tile
        case(tensor_mem_idx_tensor)
           c(i)%size_ = tensor_bytes_per_tensor
        case default 
           call tensor_status_quit("wrong index in&
              & setting up the memory counters. This is a coding error in&
              & tensor_type_def_module",202)
        end select

        !SET COUNTERS TO 0
        c(i)%curr_ = 0_tensor_long_int
        c(i)%high_ = 0_tensor_long_int

     enddo
     call tensor_stack_pop()
  end subroutine tensor_init_specific_counter


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!             DERIVED TYPE: tensor               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!                                      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!ALLOCATION INTERFACES
!subroutine tensor_allocate_tensor_1d_std(p,n1,stat)
!   implicit none
!   type(tensor), pointer, intent(inout) :: p(:)
!   integer(tensor_standard_int), intent(in)   :: n1
!   integer, intent(out), optional         :: stat
!   integer(tensor_long_int ) :: n
!
!   n = n1
!   call tensor_allocate_tensor_basic(p,n,tensor_mem_idx_tensor,stat=stat)
!
!end subroutine tensor_allocate_tensor_1d_std
!subroutine tensor_allocate_tensor_1d(p,n1,stat)
!   implicit none
!   type(tensor),pointer, intent(inout) :: p(:)
!   integer(tensor_long_int), intent(in)   :: n1
!   integer, intent(out), optional         :: stat
!   integer(tensor_long_int ) :: n
!
!   n = n1
!   call tensor_allocate_tensor_basic(p,n,tensor_mem_idx_tensor,stat=stat)
!
!end subroutine tensor_allocate_tensor_1d
!
!
!!DEALLOCATION
!subroutine tensor_free_tensor_1d(p,stat)
!   implicit none
!   type(tensor),pointer, intent(inout) :: p(:)
!   integer, intent(out), optional         :: stat
!
!   call tensor_free_tensor_basic(p,tensor_mem_idx_tensor,stat=stat)
!
!end subroutine tensor_free_tensor_1d
!
!!BASIC ALLOCATOR
!subroutine tensor_allocate_tensor_basic(p,n,idx,stat)
!   implicit none
!   type(tensor),pointer, intent(inout) :: p(:)
!
!   include "standard_allocation.inc"
!
!end subroutine tensor_allocate_tensor_basic
!!BASIC DEALLOCATOR
!subroutine tensor_free_tensor_basic(p,idx,stat)
!   implicit none
!   type(tensor),pointer, intent(inout) :: p(:)
!
!   include "standard_deallocation.inc"
!
!end subroutine tensor_free_tensor_basic
end module tensor_type_allocator_module
