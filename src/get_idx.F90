!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     June, 2015
! File:     This file contains the necessary (general mode) index manipulation
!           routines, e.g for addressing in one and multidimensional storage
!           units (PDM tensors or basic fortran arrays)
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module get_idx_mod
  use tensor_parameters_module
  
  public :: get_midx, get_cidx
  public :: get_ntpm
  public :: get_residence_of_tile_basic
  public :: get_Tsize_c, get_Tsize_m
  public :: get_Tsize_cm, get_Tsize_cc
  public :: get_Tsize_mc, get_Tsize_mm

  private

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   approximately 2013
  !Intent: interface for the calculation of a modal index (i.e. a
  !        multidimensional index) from a combined index. E.g. an 
  !        element in a column major matrix may be addressed with 
  !        c = i + (j-1) * dimi where i and j are the columnd and 
  !        row indices, respectively, and a fortran-like numbering 
  !        from 1 is assumed. This interface can recover i and j 
  !        (k...) from c for arbitrary modes.
  interface get_midx
    module procedure get_mode_idx8888,&
                    &get_mode_idx8884,&
                    &get_mode_idx8848,&
                    &get_mode_idx8844,&
                    &get_mode_idx8488,&
                    &get_mode_idx8484,&
                    &get_mode_idx8448,&
                    &get_mode_idx8444,&
                    &get_mode_idx4888,&
                    &get_mode_idx4844,&
                    &get_mode_idx4484,&
                    &get_mode_idx4444
  end interface get_midx

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   approximately 2013
  !Intent: interface for the calculation of a combined index from a 
  !        modal index (i.e. a multidimensional index). E.g. an 
  !        element in a column major matrix may be addressed with 
  !        c = i + (j-1) * dimi where i and j are the columnd and row 
  !        indices, respectively, and a fortran-like numbering from 1 
  !        is assumed. This interface calculates c from i and j (k...) 
  !        for arbitrary modes
  interface get_cidx
    module procedure get_comp_idx888,&
                    &get_comp_idx884,&
                    &get_comp_idx848,&
                    &get_comp_idx844,&
                    &get_comp_idx488,&
                    &get_comp_idx484,&
                    &get_comp_idx448,&
                    &get_comp_idx444
  end interface get_cidx

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   August 2015
  !Intent: This interface calculates the tile dimensions for a tiled 
  !        distributed tensor from a tile index which may be combined 
  !        or a mode index (see above) and returns the tile size in 
  !        individual modes. Only functions
  interface get_Tsize_m
     module procedure get_Tsize_mm, get_Tsize_mc
  end interface get_Tsize_m

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   August 2015
  !Intent: This interface calculates the tile size (combined tile 
  !        dimensions) for a tiled distributed tensor from a tile 
  !        index which may be combined  or a mode index (see 
  !        above) and returns the tile size.
  interface get_Tsize_c
     module procedure get_Tsize_cm, get_Tsize_cc
  end interface get_Tsize_c


  contains

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8888(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_long_int),intent(in)    :: modes
    integer(tensor_long_int),intent(inout) :: inds(modes)
    integer(tensor_long_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8888
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8884(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_long_int),intent(inout) :: inds(modes)
    integer(tensor_long_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8884
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8848(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_long_int),intent(in)    :: modes
    integer(tensor_long_int),intent(inout) :: inds(modes)
    integer(tensor_standard_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8848
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8844(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_long_int),intent(inout) :: inds(modes)
    integer(tensor_standard_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8844
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8488(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_long_int),intent(in)    :: modes
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    integer(tensor_long_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8488
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8484(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    integer(tensor_long_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8484
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8448(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_long_int),intent(in)    :: modes
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    integer(tensor_standard_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8448
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx8444(a,inds,dims,modes)
    implicit none
    integer(tensor_long_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    integer(tensor_standard_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx8444
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx4888(a,inds,dims,modes)
    implicit none
    integer(tensor_standard_int),intent(in)    :: a
    integer(tensor_long_int),intent(in)    :: modes
    integer(tensor_long_int),intent(inout) :: inds(modes)
    integer(tensor_long_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx4888
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx4844(a,inds,dims,modes)
    implicit none
    integer(tensor_standard_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_long_int),intent(inout) :: inds(modes)
    integer(tensor_standard_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx4844
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx4488(a,inds,dims,modes)
    implicit none
    integer(tensor_standard_int),intent(in)    :: a
    integer(tensor_long_int),intent(in)    :: modes
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    integer(tensor_long_int),intent(in)    :: dims(modes)
    include "midx.inc"
  end subroutine get_mode_idx4488
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx4484(a,inds,dims,modes)
    implicit none
    integer(tensor_standard_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_long_int),intent(in)    :: dims(modes)
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    include "midx.inc"
  end subroutine get_mode_idx4484
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get mode index from composite index
  !Input:
  !       - a:     combined index to split into the modes
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - inds:  modal index (split combined index)
  pure subroutine get_mode_idx4444(a,inds,dims,modes)
    implicit none
    integer(tensor_standard_int),intent(in)    :: a
    integer(tensor_standard_int),intent(in)    :: modes
    integer(tensor_standard_int),intent(in)    :: dims(modes)
    integer(tensor_standard_int),intent(inout) :: inds(modes)
    include "midx.inc"
  end subroutine get_mode_idx4444

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx888(inds,dims,modes) result(a)
    implicit none
    integer(tensor_long_int),intent(in) :: modes
    integer(tensor_long_int),intent(in) :: inds(modes)
    integer(tensor_long_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx888
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx884(inds,dims,modes) result(a)
    implicit none
    integer(tensor_standard_int),intent(in) :: modes
    integer(tensor_long_int),intent(in) :: inds(modes)
    integer(tensor_long_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx884
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx848(inds,dims,modes) result(a)
    implicit none
    integer(tensor_long_int),intent(in) :: modes
    integer(tensor_long_int),intent(in) :: inds(modes)
    integer(tensor_standard_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx848
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx844(inds,dims,modes) result(a)
    implicit none
    integer(tensor_standard_int),intent(in) :: modes
    integer(tensor_long_int),intent(in) :: inds(modes)
    integer(tensor_standard_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx844
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx488(inds,dims,modes) result(a)
    implicit none
    integer(tensor_long_int),intent(in) :: modes
    integer(tensor_standard_int),intent(in) :: inds(modes)
    integer(tensor_long_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx488
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx484(inds,dims,modes) result(a)
    implicit none
    integer(tensor_standard_int),intent(in) :: modes
    integer(tensor_standard_int),intent(in) :: inds(modes)
    integer(tensor_long_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx484
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx448(inds,dims,modes) result(a)
    implicit none
    integer(tensor_long_int),intent(in) :: modes
    integer(tensor_standard_int),intent(in) :: inds(modes)
    integer(tensor_standard_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx448
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   June 2012
  !Intent: get a composite index from a mode index
  !Input:
  !       - inds:  mode index
  !       - dims:  full dimensions in each mode (dimensions)
  !       - modes: number of modes (dimensions)
  !Output:
  !       - a:  combined index
  pure function get_comp_idx444(inds,dims,modes) result(a)
    implicit none
    integer(tensor_standard_int),intent(in) :: modes
    integer(tensor_standard_int),intent(in) :: inds(modes)
    integer(tensor_standard_int),intent(in) :: dims(modes)
    include "cidx.inc"
  end function get_comp_idx444

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   March 2013
  !Intent: for a tiled distributed tensor calculate the number of 
  !        tiles per mode
  !Input:
  !       - dims: full dimensions in each mode (dimensions)
  !       - tdim: requested tile size (not actual tile size)
  !       - mode: number of modes (dimensions)
  !Output:
  !       - ntpm:  number of tiles per mode
  pure function get_ntpm(dims,tdim,mode)result(ntpm)
     implicit none
     integer :: ntpm(mode)
     integer, intent(in)  :: mode
     integer, intent(in)  :: dims(mode)
     integer, intent(in)  :: tdim(mode)
     integer :: j
     do j=1,mode
        ntpm(j)=dims(j)/tdim(j)
        if(mod(dims(j),tdim(j))>0)ntpm(j)=ntpm(j)+1
     enddo
  end function get_ntpm

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   approximately March 2013
  !Intent: calculate the residence parameters for a specific tile given 
  !        a number of other parameters. The calculated parameters include 
  !        the node where the tile resides, the position of the tile on 
  !        the node the index within the allocated stretch of memory and 
  !        the index of the window within the tensor.
  !Input:
  !       - nnod:             number of nodes on the communicator with  
  !                           which the tensor is defined
  !       - offset:           offset of the initial tile (rank within the 
  !                           communicator that hosts the first tile)
  !       - tsize:            requested (not actual!!!) tile size
  !       - globaltilenumber: the global tile number
  !Output:
  !       - rank:   rank of the node hosting the tile with number
  !                 globaltilenumber (within the original communicator)
  !       - pos:    local (on rank) number of the tile
  !       - idx:    local (on rank) index of the first element of the tile 
  !                 in the window
  !       - widx:   index of the window
  pure subroutine get_residence_of_tile_basic(nnod,offset,tsize,globaltilenumber,&
        &rank,pos,idx,widx)
     implicit none
     integer(tensor_long_int), intent(in)  :: nnod
     integer(tensor_long_int), intent(in)  :: offset
     integer(tensor_long_int), intent(in)  :: tsize
     integer(tensor_long_int), intent(in)  :: globaltilenumber
     integer(tensor_long_int), intent(out) :: rank
     integer(tensor_long_int), intent(out) :: pos
     integer(tensor_long_int), intent(out) :: idx
     integer(tensor_long_int), intent(out) :: widx

     rank = mod(globaltilenumber-1+offset,nnod)

     if( alloc_in_dummy ) then
        widx = 1
        pos  = (globaltilenumber-1)/nnod + 1
        idx  = 1 + ( pos - 1 ) * tsize
     else
        widx = globaltilenumber
        pos  = 1
        idx  = 1 
     endif

  end subroutine get_residence_of_tile_basic

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   August 2015
  !Intent: calculate the modal tile size (dimensions) of a requested tile
  !Input:
  !       - tileidx: modal tile index
  !       - dims:    full dimensions in each mode (dimensions)
  !       - tdim:    requested tile size (not actual tile size)
  !       - mode:    number of modes (dimensions)
  !Output:
  !       - sze:     modal tile size (tile dimensions) 
  pure function get_Tsize_mm(tileidx,dims,tdim,mode) result(sze)
     implicit none
     integer :: sze(mode)
     integer, intent(in)  :: mode
     integer, intent(in)  :: tileidx(mode),dims(mode),tdim(mode)
     integer :: j
     !find the size of the tile in each mode
     do j=1, mode
        if(((dims(j)-(tileidx(j)-1)*tdim(j))/tdim(j))>=1)then
           sze(j)=tdim(j)
        else
           sze(j)=mod(dims(j),tdim(j))
        endif
     enddo
  end function get_Tsize_mm
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   August 2015
  !Intent: calculate the modal tile size (dimensions) of a requested tile
  !Input:
  !       - tileidx: combined tile index
  !       - dims:    full dimensions in each mode (dimensions)
  !       - tdim:    requested tile size (not actual tile size)
  !       - mode:    number of modes (dimensions)
  !Output:
  !       - sze:     modal tile size (tile dimensions) 
  pure function get_Tsize_mc(tileidx,dims,tdim,mode) result(sze)
     implicit none
     integer :: sze(mode)
     integer, intent(in)  :: mode
     integer, intent(in)  :: tileidx,dims(mode),tdim(mode)
     integer :: tidx_mode(mode),ntpm(mode)
     !Get the number of tiles in each of the modes
     ntpm = get_ntpm(dims,tdim,mode)
     !get the indices of the tile in the modes
     call get_midx(tileidx,tidx_mode,ntpm,mode)
     !get the size per mode
     sze = get_Tsize_mm(tidx_mode,dims,tdim,mode)
  end function get_Tsize_mc

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   August 2015
  !Intent: calculate the (combined) tile size of a requested tile.
  !Input:
  !       - tileidx: combined tile index
  !       - dims:    full dimensions in each mode (dimensions)
  !       - tdim:    requested tile size (not actual tile size)
  !       - mode:    number of modes (dimensions)
  !Output:
  !       - sze:     tile size
  pure function get_Tsize_cc(tileidx,dims,tdim,mode) result(sze)
     implicit none
     integer :: sze
     integer, intent(in) :: mode
     integer,intent(in) :: tileidx,dims(mode),tdim(mode)
     sze = product(get_Tsize_mc(tileidx,dims,tdim,mode))
  end function get_Tsize_cc
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   August 2015
  !Intent: calculate the (combined) tile size of a requested tile
  !Input:
  !       - tileidx: modal tile index
  !       - dims:    full dimensions in each mode (dimensions)
  !       - tdim:    requested tile size (not actual tile size)
  !       - mode:    number of modes (dimensions)
  !Output:
  !       - sze:     tile size
  pure function get_Tsize_cm(tileidx,dims,tdim,mode) result(sze)
     implicit none
     integer :: sze
     integer, intent(in) :: mode
     integer, intent(in) :: tileidx(mode),dims(mode),tdim(mode)
     sze = product(get_Tsize_mm(tileidx,dims,tdim,mode))
  end function get_Tsize_cm

end module get_idx_mod

